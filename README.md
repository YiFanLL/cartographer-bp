# cartographer-bp

#### Description
背包用cartographer

#### Software Architecture
cartographer cartographer_ros ceres-solver protobuf这四个都有

在工作空间src下clone项目，把protobuf文件夹剪切到src这一级，不然会被识别成ros包
#### 编译

1.  装一下proto3  
    cd catkin_ws

    catkin_ws/src/cartographer/scripts/install_proto3.sh 这个文件运行一下就可以

2.  安装依赖项

    sudo rosdep init

    rosdep update（连github经常会出问题，只能多换换网络试试，一般手机热点可以）

    rosdep install --from-paths src --ignore-src --rosdistro=${ROS_DISTRO} -y

3.  编译

    catkin_make_isolated --install --use-ninja

4.添加环境变量

    最后把环境变量写到bash里

    gedit ~/.bashrc

    最后一行加上

    source ~/catkin_ws/install_isolated/setup.bash

#### 使用

1.  离线节点

    roslaunch vlp16_2_offline_backpack_3d.launch bag_filenames:='bag包'

    跑完之后会生成轨迹.pbstream文件，用这个和bag包可以生成.ply

    roslaunch vlp16_2_assets_writer_backpack_3d.launch bag_filenames:='bag包' pose_graph_filename:='对应的.pbsream文件'

